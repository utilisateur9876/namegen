# NameGenerator

ce programme sert à vous générer aléatoirement un pseudo pour vous identifier sur nimporte quelle plateforme.

## pré-requis

pour pouvoir lancer le programme il faut java 1.8.0

## Installation execution du projet

mettez vous dans le répertoire du projet et les commandes à taper :

 javac sert à compiler le programme en java (il créé un fichier qui se nomera NameGenerator.class)
 java sert à lancer le programme après compilation
```bash 
javac NameGenerator.java 
java NameGenerator
```

# Pour utiliser le projet

ce programme est utilisable sous windows Linux et mac os mais il faut installer java.


## Sur d'autres plateformes ou avec une autre façon de faire

### HelloFX Sample

JavaFX 13 HelloFX sample to run with different options and build tools.

Download [JDK 11 or later](http://jdk.java.net/) for your operating system.
Make sure `JAVA_HOME` is properly set to the JDK installation directory. 

### Gradle

#### Linux / Mac

If you run on Linux or Mac, follow these steps:

    cd HelloFX/Gradle/hellofx
    
To run the project:
    
    ./gradlew run

#### Windows

If you run on Windows, follow these steps:

    cd HelloFX\Gradle\hellofx

To run the project:
    
    gradlew run
    
COMMANDE GIT A EXTRAIRE :

usage : git [--version] [--help] [-C <chemin>] [-c <nom>=<valeur>]
           [--exec-path[=<chemin>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<chemin>] [--work-tree=<chemin>] [--namespace=<nom>]
           <commande> [<args>]

Ci-dessous les commandes Git habituelles dans diverses situations :

démarrer une zone de travail (voir aussi : git help tutorial)
   clone             Cloner un dépôt dans un nouveau répertoire
   init              Créer un dépôt Git vide ou réinitialiser un existant

travailler sur la modification actuelle (voir aussi : git help revisions)
   add               Ajouter le contenu de fichiers dans l'index
   mv                Déplacer ou renommer un fichier, un répertoire, ou un lien symbolique
   restore           restaurer les fichiers l'arbre de travail
   rm                Supprimer des fichiers de la copie de travail et de l'index
   sparse-checkout   Initialiser et modifier l'extraction partielle

examiner l'historique et l'état (voir aussi : git help revisions)
   bisect            Trouver par recherche binaire la modification qui a introduit un bogue
   diff              Afficher les changements entre les validations, entre validation et copie de travail, etc
   grep              Afficher les lignes correspondant à un motif
   log               Afficher l'historique des validations
   show              Afficher différents types d'objets
   status            Afficher l'état de la copie de travail

agrandir, marquer et modifier votre historique
   branch            Lister, créer ou supprimer des branches
   commit            Enregistrer les modifications dans le dépôt
   merge             Fusionner deux ou plusieurs historiques de développement ensemble
   rebase            Réapplication des commits sur le sommet de l'autre base
   reset             Réinitialiser la HEAD courante à l'état spécifié
   switch            Basculer de branche
   tag               Créer, lister, supprimer ou vérifier un objet d'étiquette signé avec GPG

collaborer (voir aussi : git help workflows)
   fetch             Télécharger les objets et références depuis un autre dépôt
   pull              Rapatrier et intégrer un autre dépôt ou une branche locale
   push              Mettre à jour les références distantes ainsi que les objets associés

'git help -a' et 'git help -g' listent les sous-commandes disponibles et
quelques concepts. Voir 'git help <commande>' ou 'git help <concept>'
pour en lire plus à propos d'une commande spécifique ou d'un concept.
Voir 'git help git' pour un survol du système.
