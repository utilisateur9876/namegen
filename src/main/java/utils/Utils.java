package utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {
    /**
     * Méthode permetant de créer un nombre aléatoire.
     * @return dice int
     */
    public static Integer randDice(){
        // On affecte le chiffre aléatoire
        int dice = (int) (Math.random() * 6);

        // On gère le cas ou le 0 arrive alors on recommence le randome jusqu'à un autre nombre que 0
        while (dice == 0) {
            dice = (int) (Math.random() * 6);
        }
        System.out.println("The dice = " + dice);

        // On retourn une valeur entre 1 et 6
        return dice;
    }

    /**
     * Méthode permettant d'optmiser l'utilisation du switch case avec une list de string et d'index pour rendre la liste dynamique
     * @param syllables morceau de texte choisi
     * @param secondPart seconde partie du mot final
     * @param sequenceList la séquence que nous souhaitons utilisé pour les probabilités
     * @return un string avec le mot ou vide si pas de case
     */
    public static String stringProbabilitys(List<String> syllables, String secondPart, List<Integer> sequenceList) {

        switch (randDice()) {
            case 1:
                System.out.println("syllabe : " + syllables.get(0));
                return syllables.get(sequenceList.get(0));
            case 2:
                System.out.println("syllabe : " + syllables.get(1));
                return syllables.get(sequenceList.get(1));
            case 3:
                System.out.println("syllabe : " + syllables.get(2));
                return syllables.get(sequenceList.get(2));
            case 4:
                System.out.println("syllabe : " + syllables.get(3));
                return syllables.get(sequenceList.get(3));
            case 5:
                System.out.println("syllabe : " + syllables.get(4));
                return syllables.get(sequenceList.get(4));
            case 6:
                System.out.println("syllabe : " + syllables.get(5));
                return syllables.get(sequenceList.get(5));
            default:
                System.out.println("1- Le dé n'est pas géré.");
                return "";
        }
    }
}
