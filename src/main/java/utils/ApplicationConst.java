package utils;

public final class ApplicationConst {

    /**
     * Infofmations de l'application
     */
    public static final String APP_NAME = " NameGenerator ";
    public static final String APP_ENV = " dev ";
    public static final String APP_VERSION = " 1.0.0 ";

    /**
     * mot utilisé pour composé le pseudo
     */
    public static final String FIRST_SYLLABLE = "al";
    public static final String SECOND_SYLLABLE = "ex";
    public static final String THREE_SYLLABLE = "an";
    public static final String FOUR_SYLLABLE = "dre";
    public static final String FIVE_SYLLABLE = "roff";
    public static final String SIX_SYLLABLE = "iaen";
    public static final String EMPTY = "";

    /**
     * Message affiché en console
     */
    public static final String MSG_CASE_1 = "premiere sylabe = ";
    public static final String MSG_CASE_2 = "seconde sylabe = ";
    public static final String MSG_CASE_3 = "troieme sylabe = ";
    public static final String MSG_CASE_4 = "quatrieme sylabe = ";
    public static final String MSG_CASE_5 = "cinquieme sylabe = ";
    public static final String MSG_CASE_6 = "sixieme sylabe = ";
    public static final String MSG_CASE_DEFAULT = "Le choix n'est pas implémnter";

    /**
     * String utilisé à la fin du programme pour le rendu utilisateur
     */
    public static final String MSG_PSEUDO= "Votre pseudo : ";


}
