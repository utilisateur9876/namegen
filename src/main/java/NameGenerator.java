import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import utils.ApplicationConst;

import java.util.ArrayList;
import java.util.List;

/**
 * Méthodes que j'ai isolé dans un autre fichier Java pour optimiser la lecture.
 */
import static utils.Utils.randDice;
import static utils.Utils.stringProbabilitys;

public class NameGenerator extends Application {

    /**
     * La variation du code nécéssite une seul varible rendre une mécanique plus de ce coté la dans l'algo
     * @param stage objet pour l'affichage du programme
     */
    @Override
    public void start(Stage stage) {

        System.out.println("");
        System.out.println("███╗   ██╗ █████╗ ███╗   ███╗███████╗ ██████╗ ███████╗███╗   ██╗");
        System.out.println("████╗  ██║██╔══██╗████╗ ████║██╔════╝██╔════╝ ██╔════╝████╗  ██║");
        System.out.println("██╔██╗ ██║███████║██╔████╔██║█████╗  ██║  ███╗█████╗  ██╔██╗ ██║");
        System.out.println("██║╚██╗██║██╔══██║██║╚██╔╝██║██╔══╝  ██║   ██║██╔══╝  ██║╚██╗██║");
        System.out.println("██║ ╚████║██║  ██║██║ ╚═╝ ██║███████╗╚██████╔╝███████╗██║ ╚████║");
        System.out.println("╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝ ╚═════╝ ╚══════╝╚═╝  ╚═══╝");
        System.out.println("#");
        System.out.println("#" + ApplicationConst.APP_NAME + ApplicationConst.APP_VERSION);
        System.out.println("#-----------------------------------------------------------");
        System.out.println("");

        /**
         * Init des lists et variables du programme
         */
        List<String> syllables = new ArrayList<>();
        syllables.add(ApplicationConst.FIRST_SYLLABLE);
        syllables.add(ApplicationConst.SECOND_SYLLABLE);
        syllables.add(ApplicationConst.THREE_SYLLABLE);
        syllables.add(ApplicationConst.FOUR_SYLLABLE);
        syllables.add(ApplicationConst.FIVE_SYLLABLE);
        syllables.add(ApplicationConst.SIX_SYLLABLE);

        String firstPart = ApplicationConst.EMPTY;
        String secondPart = syllables.get(0);

        /**
         * Utilisation de 6 séquences dans 6 liste pour faire varier le contenu
         */
        // Première séquence des probabilités
        List<Integer> firstSequence = SequenceList.first(new ArrayList<>());
        List<Integer> secondSequence = SequenceList.second(new ArrayList<>());
        List<Integer> threeSequence = SequenceList.three(new ArrayList<>());
        List<Integer> fourSequence = SequenceList.four(new ArrayList<>());
        List<Integer> fiveSequence = SequenceList.five(new ArrayList<>());
        List<Integer> sixSequence = SequenceList.six(new ArrayList<>());

        System.out.println("Première syllabe = " + secondPart);

        /**
         * début du programme 1er partie
         */
        switch(randDice()) {
            case 1:

                secondPart = syllables.get(0);
                System.out.println(ApplicationConst.MSG_CASE_1 + secondPart);

                // Premier switchcase optimisé.
                firstPart = stringProbabilitys(syllables, secondPart, firstSequence);
                break;
            case 2:

                secondPart = syllables.get(1);
                System.out.println(ApplicationConst.MSG_CASE_2 + secondPart);

                // Second switchcase optimisé.
                firstPart = stringProbabilitys(syllables, secondPart, secondSequence);
                break;
            case 3:

                secondPart = syllables.get(2);
                System.out.println(ApplicationConst.MSG_CASE_3 + secondPart);

                // troiseme switchcase optimisé.
                firstPart = stringProbabilitys(syllables, secondPart, threeSequence);
                break;
            case 4:

                secondPart = syllables.get(3);
                System.out.println(ApplicationConst.MSG_CASE_4 + secondPart);

                // quatrieme switchcase optimisé.
                firstPart = stringProbabilitys(syllables, secondPart, fourSequence);
            case 5:

                secondPart = syllables.get(4);
                System.out.println(ApplicationConst.MSG_CASE_5 + secondPart);

                // cinquieme switchcase optimisé.
                firstPart = stringProbabilitys(syllables, secondPart, fiveSequence);
                break;
            case 6:

                secondPart = syllables.get(5);
                System.out.println(ApplicationConst.MSG_CASE_6 + secondPart);

                // sixeme switchcase optimisé.
                firstPart = stringProbabilitys(syllables, secondPart, sixSequence);
                break;
            default:
                System.out.println(ApplicationConst.MSG_CASE_DEFAULT);
        }

        /**
         * Résultat et affichage à l'utilisateur
         */
        Label l = new Label(ApplicationConst.MSG_PSEUDO+ secondPart + firstPart);
        Scene scene = new Scene(new StackPane(l), 350, 100);
        stage.setTitle(ApplicationConst.APP_NAME + ApplicationConst.APP_VERSION);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}