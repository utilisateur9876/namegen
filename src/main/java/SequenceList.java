import java.util.List;

public class SequenceList {

    /**
     * Premiere liste de sequence
     * @param listSeq la séquence voulu pour l'algo
     * @return listSeq
     */
    public static List<Integer> first(List<Integer> listSeq) {
        listSeq.add(0);
        listSeq.add(1);
        listSeq.add(2);
        listSeq.add(3);
        listSeq.add(4);
        listSeq.add(5);

        return listSeq;
    }

    /**
     * Seconde liste de sequence
     * @param listSeq la séquence voulu pour l'algo
     * @return listSeq
     */
    public static List<Integer> second(List<Integer> listSeq) {
        listSeq.add(5);
        listSeq.add(4);
        listSeq.add(3);
        listSeq.add(2);
        listSeq.add(1);
        listSeq.add(0);
        return listSeq;
    }

    /**
     * Troisème liste de sequence
     * @param listSeq la séquence voulu pour l'algo
     * @return listSeq
     */
    public static List<Integer> three(List<Integer> listSeq) {
        listSeq.add(5);
        listSeq.add(5);
        listSeq.add(2);
        listSeq.add(4);
        listSeq.add(0);
        listSeq.add(3);
        return listSeq;
    }

    /**
     * Quatrième liste de sequence
     * @param listSeq la séquence voulu pour l'algo
     * @return listSeq
     */
    public static List<Integer> four(List<Integer> listSeq) {
        listSeq.add(2);
        listSeq.add(3);
        listSeq.add(1);
        listSeq.add(0);
        listSeq.add(5);
        listSeq.add(4);
        return listSeq;
    }

    /**
     * Cinquième liste de sequence
     * @param listSeq la séquence voulu pour l'algo
     * @return listSeq
     */
    public static List<Integer> five(List<Integer> listSeq) {
        listSeq.add(3);
        listSeq.add(0);
        listSeq.add(5);
        listSeq.add(4);
        listSeq.add(2);
        listSeq.add(1);
        return listSeq;
    }

    /**
     * Sixième liste de sequence
     * @param listSeq la séquence voulu pour l'algo
     * @return listSeq
     */
    public static List<Integer> six(List<Integer> listSeq) {
        listSeq.add(4);
        listSeq.add(5);
        listSeq.add(0);
        listSeq.add(1);
        listSeq.add(3);
        listSeq.add(2);
        return listSeq;
    }
}
